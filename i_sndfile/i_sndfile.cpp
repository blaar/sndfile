#include <sndfile.h>

#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h> //getpid

#define DEFAULT_OUTPUT_NAME ":sound<pid>" //':'

int main(int argc, char**argv){
    blc_channel output;
    char const *filename, *output_name, *type_str, *length_str, *period_str, *samplerate_str;
    int period, samplerate;
    size_t length;
    uint32_t type;
    SF_INFO sfinfo;
    SNDFILE *sndfile;
    sf_count_t read_items_nb;
    
    blc_program_set_description("Record blc_channel input in a sound file");
    blc_program_add_option(&filename, 'f', "file", "string", "filename of the file to write", NULL);
    blc_program_add_option(&output_name, 'o', "output", "string", "Channel containing the fast fourier transformation", DEFAULT_OUTPUT_NAME);
    blc_program_add_option(&period_str, 'p', "period", "integer", "Period between each sample in ms (0 as fast as possible)", "0");
    blc_program_add_option(&length_str, 's', "size", "integer", "Sample size", "4096");
    blc_program_add_option(&samplerate_str, 'S', "samplerate", "integer", "frequency of sampling", "44100");
    blc_program_add_option(&type_str, 't', "type", "INT8|FL32", "Type of the data.", "FL32");
    blc_program_init(&argc, &argv, blc_quit);
    
    SSCANF(1, samplerate_str, "%d", &samplerate);

    if (filename==NULL){
    	blc_program_args_display_help();
    	EXIT_ON_ERROR("You need to specify a filename: -f<filename>");
    }

    type=STRING_TO_UINT32(type_str);
    if (sscanf(length_str, "%ld", &length)!=1) EXIT_ON_ERROR("Reading size '%s'", length_str);
    if (sscanf(period_str, "%d", &period)!=1)  EXIT_ON_ERROR("Reading period in '%s'", period_str);
    
    if (strcmp(DEFAULT_OUTPUT_NAME, output_name)==0) asprintf((char**)&output_name, ":sound%d", getpid());

    output.create_or_open(output_name, BLC_CHANNEL_WRITE, type, 'LPCM', 1, length);
    output.publish();
    
    CLEAR(sfinfo);
    sndfile=sf_open(filename, SFM_READ,  &sfinfo);
 //   if (sfinfo.format!=samplerate) EXIT_ON_ERROR("The file has been recorded with a samplerate of '%d' and you request '%d'", sfinfo.samplerate, samplerate);
    if (sndfile==NULL) EXIT_ON_ERROR("Opening '%s'. libsndfile: '%s'", filename, sf_strerror(NULL));
    
    blc_loop_try_add_waiting_semaphore(output.sem_ack_data);
    blc_loop_try_add_posting_semaphore(output.sem_new_data);

    BLC_COMMAND_LOOP(period*1000){
        read_items_nb=sf_read_float(sndfile, output.floats, output.total_length);
        if (read_items_nb!=output.total_length) {
           color_eprintf(BLC_YELLOW, "%s: Iteration %d: Missing data. Read '%lld' items instead of '%ld'. It is probably the end of the file\n", blc_program_name, blc_loop_iteration,  read_items_nb, output.total_length);
            blc_command_ask_quit();
            break;
        }
    }
    
    sf_close(sndfile);
    return EXIT_SUCCESS;
}
