cmake_minimum_required(VERSION 2.6)
project(o_sndfile)


find_path(SNDFILE_INCLUDE_DIR sndfile.h)
find_library(SNDFILE_LIBRARIES NAMES sndfile)

find_package(blc_channel REQUIRED)
find_package(blc_program REQUIRED)

add_definitions(${BL_DEFINITIONS})
include_directories(${BL_INCLUDE_DIRS} ${SNDFILE_INCLUDE_DIR})
add_executable(o_sndfile o_sndfile.cpp)
target_link_libraries(o_sndfile ${BL_LIBRARIES} ${SNDFILE_LIBRARIES})
