#include <sndfile.h>

#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h> //getpid

int main(int argc, char**argv){
    blc_channel input;
    char const *filename, *input_name, *samplerate_str, *output_length_str;
    SF_INFO sfinfo;
    SNDFILE *sndfile;
    sf_count_t write_items_nb;
    int samplerate, length;
    
    blc_program_set_description("Load a sound file (no mp3) and put the result in the blc_channel");
    blc_program_add_option(&filename, 'f', "file", "string", "filename of the file to write", NULL);
    blc_program_add_option(&output_length_str, 's', "size", "integer", "items nb in buffer", "4096");
    blc_program_add_option(&samplerate_str, 'S', "samplerate", "integer", "frequency of sampling", "44100");
    blc_program_add_parameter(&input_name, "blc_channel-in", 1, "blc channel containing the sound", NULL);
    blc_program_init(&argc, &argv, blc_quit);
    
    SSCANF(1, samplerate_str, "%d", &samplerate);
    SSCANF(1, output_length_str, "%d", &length);

    if (filename==NULL) {
        blc_program_args_display_help();
        color_eprintf(BLC_RED, "You need to set a filename with option --file=\n");
        exit(1);
    }
    
    input.open(input_name, BLC_CHANNEL_READ);

    CLEAR(sfinfo);
    sfinfo.format=SF_FORMAT_WAV | SF_FORMAT_FLOAT;
    sfinfo.channels=1;
    sfinfo.samplerate=samplerate;
    
    sndfile=sf_open(filename, SFM_WRITE, &sfinfo);
    if (sndfile==NULL) EXIT_ON_ERROR("Opening '%s'. libsndfile: '%s'", filename, sf_strerror(NULL));
    
    blc_loop_try_add_waiting_semaphore(input.sem_new_data);
    blc_loop_try_add_posting_semaphore(input.sem_ack_data);

    BLC_COMMAND_LOOP(0){
        write_items_nb=sf_write_float(sndfile, input.floats, input.total_length);
        if (write_items_nb!=input.total_length) EXIT_ON_ERROR("Iteration %d: Missing data. Read '%d' items instead of '%d'", blc_loop_iteration,  write_items_nb, input.total_length);
    }
    
    sf_close(sndfile);
    return EXIT_SUCCESS;
}
